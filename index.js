/*
    These codes are not copy pasted.
*/

document.addEventListener("keydown", function(){
    getDirection(event);
});

// Game Sound Effects
const ateSound = document.getElementById("pop");
const bgSound = document.getElementById("bgMusic");
const loseSound = document.getElementById("tonk");
    bgSound.volume="0.05";
    ateSound.volume="0.05";
    loseSound.volume="0.1";

// Directions
const WEST = 37;
const NORTH = 38;
const EAST = 39;
const SOUTH = 40;
const PAUSE = 80;
const START = 13;
let PREV = EAST;
let shift = false;

// Snake attributes
let snakeSpeed = 120;
let snake = 0;
let blockNum = 392;
let blockList = [];
let tailIndex = 0;
let direction = EAST;
let ratAlive = false;
let ratPosition;
let denom = 1;

// snake colors
// For random color of Snake
let snakeColor = ['blue', 'darkblue', 'green',
 'orange', 'red', 'purple', 'darkred', 'darkslategray',
'brown'];
let colorIndex = Math.floor(Math.random() * snakeColor.length);

//For rat Colors
// Random rat colors
let ratColor = ['url(images/dog.png)', 'url(images/pig.png)', 'url(images/rat.png)'
,'url(images/cat.png)', 'url(images/frog.png)'
, 'url(images/minionPig.png)' ];

//logic
let loss = false;

// change color combination
let snakeBody = snakeColor[colorIndex];
let gameBoard = 'white';

// snake Scores
let totalScore = 0;
let preyEaten = 0;
let preyScore = 100;

//
let doubleKey = false;
let currentSpeed = snakeSpeed;


//Board Block Number
const boardBlock = 800;

gameStart();
if(preyEaten === 2) {
    spawnStones(1);
}

function gameStart() {
    createBoard();
    createSnake();
}

// creating Game board
function createBoard() {
    for(let count = 1; count <= boardBlock; count++) {
        let childDiv = document.createElement("div");
        childDiv.style.backgroundColor=gameBoard;             
        childDiv.setAttribute('class', 'blocks');
        childDiv.setAttribute('id', count);
        //childDiv.innerHTML = count;
        let parent = document.getElementById('boardPanel');
        parent.appendChild(childDiv);
        if(count % 32 === 0) {
            childDiv.style.marginRight = "0px";
        }
        if(count > 32) {
            childDiv.style.marginTop = "1px";
        }
    }
}

// creating initail snake length, then start moving.
function createSnake() {
    if(snake > 4) {
        if(!ratAlive) {
            respawnRat();
        }
        bgSound.pause();
        bgSound.currentTime="0";
        snakeShift();
    } else {
        let block = document.getElementById(blockNum);

        block.style.backgroundImage="url(images/rightEyes.png)";
        block.style.borderTopRightRadius="5px";
        block.style.borderBottomRightRadius="5px";
        block.style.backgroundColor = snakeBody;
        blockList.push(blockNum);
        if(snake >= 1) {
            document.getElementById(blockList[snake - 1]).style.backgroundImage="";
            document.getElementById(blockList[snake - 1]).style.borderRadius="0px";
        }
        snake++;
        blockNum++;
    }

    // for interval of snake movement
    if(!loss) {
        timeout = setTimeout(createSnake, snakeSpeed);
    } else {
        document.getElementById("popDiv").style.display="block";
        document.getElementById("body").style.backgroundColor="white";
        loseSound.play();
        bgSound.pause();
    }
}

// respawning a rat
function respawnRat() {
    let colorRatIndex = Math.floor(Math.random() * ratColor.length);
    let rat = ratColor[colorRatIndex];
    if(rat === 'url(images/minionPig.png)') {
        preyScore = 60;
    } else if(rat === 'url(images/pig.png)') {
        preyScore = 200;
    } else if(rat === 'url(images/cat.png)') {
        preyScore = 90;
    } else if(rat === 'url(images/rat.png)') {
        preyScore = 80;
    } else if(rat === 'url(images/frog.png)') {
        preyScore = 70;
    } else {
        preyScore = 100;
    }
    ratPosition = Math.floor(Math.random() * boardBlock) + 1;
    let block = document.getElementById(ratPosition);
    if(block.style.backgroundColor === snakeBody 
        || (ratPosition === blockList[tailIndex] || ratPosition === blockList[tailIndex - 1])) {
        respawnRat();
    } else if(block.style.backgroundColor === 'rgb(249, 249, 249)') {
        respawnRat();
    } else {
        block.style.backgroundColor="";
        block.style.backgroundImage = rat;
        ratAlive = true;
    }
}

// spawning obstacle stones
function spawnStones(stoneNum) {
    let stonePosition = Math.floor(Math.random() * boardBlock) + 1;
    let i = blockList[blockList.length - 1];
    let list = [];
    let index = 0;
    while(true) {
        i += denom;
        list[index] = i;
        if((isInEastBorder(i) || isInWestBorder(i)) || (isInSouthBorder(i) || isInNorthBorder(i))) {
            break;
        }
        index++;
    }
    for(let x = 0; x < list.length; x++) {
        if(stonePosition === list[x]) {
            stonePosition = Math.floor(Math.random() * boardBlock) + 1;
            x = -1;
        }
    }
    let block = document.getElementById(stonePosition);
    if(stoneNum === 0) {
        return;
    }
    if(block.style.backgroundColor === snakeBody || ratPosition === blockList[tailIndex]) {
        spawnStones(stoneNum);
    } else if(block.style.backgroundImage) {
        spawnStones(stoneNum);
    }else {
        block.style.backgroundColor='rgb(249, 249, 249)';
        block.style.backgroundImage = "url(images/blueStone.png)";
        spawnStones(stoneNum - 1);
    }
}

// snake Shifts according to direction
function snakeShift() {
    
    if(doubleKey) {
        //currentSpeed = snakeSpeed;
        snakeSpeed = currentSpeed;
        doubleKey = false;
    }
    switch(direction) {

        //if the direction is WEST
        case WEST: {
            denom = -1;
            let path = blockList[blockList.length - 1] + denom;
            if(isInEastBorder(path) || path <= 0) {
                loss = true;
                break;
            } else {
                snakeMove(path);
                break;
            }
        }

        //if the direction is NORTH
        case NORTH: {
            denom = -32;
            let path = blockList[blockList.length - 1] + denom;
            if(path <= 0) {
                loss = true;
                break;
            } else {
                snakeMove(path);
                break;
            }
        }

        //if the direction is EAST
        case EAST: {
            denom = 1;
            let path = blockList[blockList.length - 1] + denom;
            if(isInWestBorder(path) || path >= boardBlock + 1) {
                loss = true;
                break;
            } else {
                snakeMove(path);
                break;
            }
        }

        //if the direction is SOUTH
        case SOUTH: {
            denom = 32;
            let path = blockList[blockList.length - 1] + denom;
            if(path >= boardBlock + 1) {
                loss = true;
                break;
            } else {
                snakeMove(path);
                break;
            }
        }

        //for catching bugs
        default: {
            break;
        }
    }
}

// moving snake by 1 tile
function snakeMove(path) {
    document.getElementById("bgMusic").play();
    let headBlock = document.getElementById(path);
    let head = document.getElementById(blockList[blockList.length - 1]);
    let tailBlock = document.getElementById(blockList[tailIndex]);  
    
    //For handling two key press, bug
    if(path === blockList[blockList.length - 2]) {
        switch(direction) {
            case WEST: {
                direction = EAST;
                break;
            } 
            case NORTH: {
                direction = SOUTH;
                break;
            }
            case EAST: {
                direction = WEST;
                break;
            }
            case SOUTH: {
                direction = NORTH;
                break;
            }
        }
        currentSpeed = snakeSpeed;
        snakeSpeed = 1;
        doubleKey = true;
        return false;
    }

    //if the snake eats its own body, head collide with the snake body.
    //if collided with stone obstacle
    if(headBlock.style.backgroundColor === snakeBody
        || headBlock.style.backgroundColor === 'rgb(249, 249, 249)') {
        loss = true;
        return false;
    }

    if((snake + 1) % 13 === 0 && headBlock.style.backgroundImage) {
        spawnStones(1);
    }
    
    //if the snake eats the prey
    if(headBlock.style.backgroundImage) {
        ateSound.play();
        ratAlive = false;
        preyEaten++;
        if(snakeSpeed > 85) {
            snakeSpeed -= (snakeSpeed * 0.015);
        }
        if(preyEaten % 8 === 0 && preyEaten !== 0) {
            spawnStones(1);
        }
        if(preyEaten === 4) {
            spawnStones(1);
        }
        totalScore += preyScore;
        document.getElementById("totalScore").innerHTML = totalScore;
        document.getElementById("preyEaten").innerHTML = preyEaten;
        tailIndex--;
        snake++;
    }

    //for adding eyes to snake head
    headBlock.style.backgroundColor = snakeBody;
    head.style.borderRadius="0px";
    if(direction == EAST) {
        headBlock.style.backgroundImage="url(images/rightEyes.png)";
        headBlock.style.borderTopRightRadius="5px";
        headBlock.style.borderBottomRightRadius="5px";
        
        //For smooth corners of the snake, if shifting direction
        if(shift) {
            if(PREV === NORTH) {
                head.style.borderTopLeftRadius="5px";
            } else if(PREV === SOUTH) {
                head.style.borderBottomLeftRadius="5px";
            }
            shift = false;
        }
    }else if(direction == NORTH) {
        headBlock.style.backgroundImage="url(images/topEyes.png)";
        headBlock.style.borderTopRightRadius="5px";
        headBlock.style.borderTopLeftRadius="5px";

        //For smooth corners of the snake, if shifting direction
        if(shift) {
            if(PREV === EAST) {
                head.style.borderBottomRightRadius="5px";
            } else if(PREV === WEST) {
                head.style.borderBottomLeftRadius="5px";
            }
            shift = false;
        }        
    } else if(direction == WEST) {
        headBlock.style.backgroundImage="url(images/leftEyes.png)";
        headBlock.style.borderTopLeftRadius="5px";
        headBlock.style.borderBottomLeftRadius="5px";

        //For smooth corners of the snake, if shifting direction
        if(shift) {
            if(PREV === NORTH) {
                head.style.borderTopRightRadius="5px";
            } else if(PREV === SOUTH) {
                head.style.borderBottomRightRadius="5px";
            }
            shift = false;
        }
    } else if(direction == SOUTH){
        headBlock.style.backgroundImage="url(images/bottomEyes.png)";
        headBlock.style.borderBottomLeftRadius="5px";
        headBlock.style.borderBottomRightRadius="5px";

        //For smooth corners of the snake, if shifting direction
        if(shift) {
            if(PREV === EAST) {
                head.style.borderTopRightRadius="5px";
            } else if(PREV === WEST) {
                head.style.borderTopLeftRadius="5px";
            }
            shift = false;
        }
    }
    head.style.backgroundImage="";
    tailBlock.style.backgroundColor = gameBoard;
    tailBlock.style.borderRadius="0px";
    blockList.push(path);
    tailIndex++;

    //updating prey score
    if(preyScore !== 0) {
        preyScore--;
    }
    document.getElementById("preyScore").innerHTML = preyScore;
}


// Checks if the snake touch the east border of the board.
function isInEastBorder(path) {
    for(let i = 32; i <= boardBlock + 1; i += 32) {
        if(path === i) {
            return true;
        }
    }
    return false;
}

// Checks if the snake touch the west border of the board.
function isInWestBorder(path) {
    for(let i = 1; i <= 769; i += 32){
        if(path === i) {
            return true;
        }
    }
    return false;
}

// Checks if the snake touch the north border
function isInSouthBorder(path) {
    for(let i = 1; i <= 32; i++){
        if(path === i) {
            return true;
        }
    }
    return false;
}

// Checks if the snake touch the north border
function isInNorthBorder(path) {
    for(let i = 769; i <= 800; i++){
        if(path === i) {
            return true;
        }
    }
    return false;
}

// Getting direction from the keys...
function getDirection(event) {
    let ev = event.keyCode || event.key;
    switch(ev) {

        //if the keypress is left arrow key
        case 0x7B: // Left key, Mac
        case 0x25: // Left key, Windows
        case 65: // A
        case 0x41: // A, windows
        case WEST: {
            if(direction != EAST) {
                PREV = direction;
                shift = true;
                direction = WEST;
            }
            break;
        }

        //if the keypress is Up arrow key
        case 0x7E: // UP key, Mac
        case 0x26: // UP key, windows
        case 87: // W
        case 0x57: // W, windows
        case NORTH: {
            if(direction != SOUTH) {
                PREV = direction;
                shift = true;
                direction = NORTH;
            }
            break;
        }

        //if the keypress is Right arrow key
        case 0x7C: // Right key, Mac
        case 0x27: // Right key, windows
        case 68: // D
        case 0x44: // D, windows
        case EAST: {
            if(direction != WEST) {
                PREV = direction;
                shift = true;
                direction = EAST;
            }
            break;
        }

        //if the keypress is Down arrow key
        case 0x7D: // Down key, Mac
        case 0x28: // Down key, Windows
        case 83: // S
        case 0x53: // S, windows
        case SOUTH: {
            if(direction != NORTH) {
                PREV = direction;
                shift = true;
                direction = SOUTH;
            }
            break;
        }

        //for catching bugs
        default: {
            break;
        }
    }
}
